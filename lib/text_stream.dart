import 'dart:async';

class TextStream {

  Stream<String> getText() async* {
    final List<String> texts = [ "Hello", "from", "the", "other", "side"];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t % 5;
      return texts[index];
    });
  }
}
