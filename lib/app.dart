import 'package:flutter/material.dart';
import 'package:week3_ex1/text_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}
class HomePageState extends State<StatefulWidget> {
  TextStream textStream = TextStream();
  List<String> texts= [];
  @override
  void initState(){
    changeText();
    super.initState();
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Text',
      home: Scaffold(
        appBar: AppBar(title: Text('Change Text'),),
        body: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: texts.length,
          itemBuilder: (BuildContext context, int index){
            return (Text(texts[index]));
          },
        ),
      ),
    );
  }

  changeText() async {
    textStream.getText().listen((eventText) {
      setState(() {
        texts.add(eventText);
      });
    });
  }
}